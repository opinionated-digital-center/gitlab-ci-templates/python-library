# GitLab CI Templates for Python Library or CLI Projects

[![](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/badges/master/pipeline.svg)](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/pipelines)
[![](https://img.shields.io/badge/license-MIT-blue)](https://choosealicense.com/licenses/mit/)

Example usage:

```yaml
include:
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-unit-test-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-bdd-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-format-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-lint-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.test-stage-type-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.release-stage-job.yml"
  - remote: "https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/raw/v0.2.0/templates/.release-stage-semantic-release-job.yml"

variables:
  GIT_STRATEGY: clone

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH == "release"
      when: always
    - if: $CI_MERGE_REQUEST_ID
      when: always
    - if: $CI_COMMIT_TAG
      when: never

default:
  image: python:3.7

stages:
  - test
  - release

#################################################################
# test stage jobs
#################################################################

py38:
  extends: .test-stage-unit-test-job
  # Override with commands of your choice
  before_script:
    - echo "foo"
    - make setup-cicd-release-stage
    - . $HOME/.poetry/env
  image: python:3.8

py37:
  extends: .test-stage-unit-test-job
  image: python:3.7

bdd-py38:
  extends: .test-stage-bdd-job
  image: python:3.8

format:
  extends: .test-stage-format-job

lint:
  extends: .test-stage-lint-job

type:
  extends: .test-stage-type-job

#################################################################
# release stage jobs
#################################################################

semantic_release:
  extends: .release-stage-semantic-release-job
```
