"""Console script for test_4_python_library_gitlab_ci_template."""

import sys

import cleo

from . import __version__
from .core import hello_world


class HelloCommand(cleo.Command):
    """
    First Command

    hello
    """

    def handle(self):
        self.line(hello_world())


class Application(cleo.Application):
    def __init__(self):
        super().__init__(
            "Test project for the Python Libraries' GitLab CI Template", __version__
        )

        self.add(HelloCommand())


def main(args=None):
    return Application().run()


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
