========================================================================
Introdution to Test project for the Python Libraries' GitLab CI Template
========================================================================

.. image:: https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/badges/master/pipeline.svg
    :target: https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/pipelines

.. image:: https://readthedocs.org/projects/test-project/badge/?version=latest
        :target: https://test-project.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

Test project for the Python Libraries' GitLab CI Template.

* Free software license: MIT
* Documentation: https://test-project.readthedocs.io

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the
`opinionated-digital-center/python-library-project-generator`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opinionated-digital-center/python-library-project-generator`: https://github.com/opinionated-digital-center/python-library-project-generator
