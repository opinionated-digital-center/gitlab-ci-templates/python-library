#!/usr/bin/env python
"""Tests for `test_4_python_library_gitlab_ci_template` package."""

from test_4_python_library_gitlab_ci_template import core


def test_hello_world():
    """Test hello world."""
    assert core.hello_world() == "Hello world!"
