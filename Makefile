.PHONY: *

#################################################################
# setting up dev env
#################################################################

setup-dev-env-full: clean
	poetry install --no-root

setup-dev-host:
	./scripts/install_pyenv.sh
	./scripts/install_poetry.sh
	@echo "Host setup correctly. Restart your shell or source your shell config file to be up and running :)"

setup-release-tools:
	npm install -g semantic-release@"^17.0.4"
	npm install -g @semantic-release/changelog@"^5.0.1"
	npm install -g @semantic-release/exec@"^5.0.0"
	npm install -g @semantic-release/git@"^9.0.0"
	npm install -g @semantic-release/gitlab@"^6.0.3"

#################################################################
# setting dependencies for automation (tox, cicd)
#################################################################

install-test-dependencies:
	poetry install --no-root

#################################################################
# setting up ci-cd env
#################################################################

setup-cicd-python3:
	update-alternatives --install /usr/bin/python python /usr/bin/python3 1
	curl -sSL  https://bootstrap.pypa.io/get-pip.py | python

setup-cicd-poetry:
	curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
	. $(HOME)/.poetry/env && poetry config virtualenvs.create false
	echo "WARNING: you still need to source $$HOME/.poetry/env to access poetry's executable"

setup-cicd-test-stage: setup-cicd-poetry

setup-cicd-release-stage: setup-cicd-python3 setup-cicd-poetry setup-release-tools

#################################################################
# cleaning
#################################################################

clean: clean-pyc

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

#################################################################
# template pipelines/workflows testing
#################################################################

test:
	poetry run pytest tests

#################################################################
# releasing (full cycle)
#################################################################

cicd-release:
	npx semantic-release

#################################################################
# git targets
#################################################################

prune-branches:
	git remote prune origin
	git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $$1; }' | xargs git branch -d


prune-branches-force:
	git remote prune origin
	git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $$1; }' | xargs git branch -D

pbf: prune-branches-force

post-PR-merge-sync-step-1:
	git switch master
	git pull

pms: post-PR-merge-sync-step-1 prune-branches-force
