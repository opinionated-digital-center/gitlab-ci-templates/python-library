# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/compare/v0.1.0...v0.2.0) (2020-06-11)


### Features

* rename files to clarify ([22281d9](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/commit/22281d96bf2223382a91fc20e3fbd059d84cf960))
* rename templates for clarity ([ade07a3](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/commit/ade07a3d370841d7011c2b5d169815f25e2f4102))

## [0.1.0](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/compare/v0.0.0...v0.1.0) (2020-04-30)


### Features

* first working release ([bbf9b15](https://gitlab.com/opinionated-digital-center/gitlab-ci-templates/python-library/commit/bbf9b15feaf8e4b929906fa4d1f0b78bb7097995))
